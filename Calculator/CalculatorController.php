<?php

class CalculatorController {
    public function processRequest($operator, $value) {
        $execute = new Calculator;        
        $execute->operator = $operator;
        $execute->setValue($value);    
        $execute->calculate();
    } 
}