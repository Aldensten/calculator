<?php

declare(strict_types=1);

use PHPUnit\Framework\TestCase;

class OperatorTest extends TestCase {
    public function testCorrectValue() {
        $this->assertEquals(2, Operator::_add(1,1));
    }
    public function testCorrectDataType() {
        $this->assertEquals('integer', gettype(Operator::_add(1,1)));
    }
    public function testIsFloat() {
        $this->assertTrue(true, is_float(Operator::_add(1.2,1)));
    }
    public function testIsValueNegative() {
        $this->assertEquals(-10, Operator::_add(-10,0));
    }
    public function testValidStringValue() {
        $this->assertEquals(4, Operator::_add('2','2'));
    }
}