<?php
require_once('Autoloader.php');
$load = new Autoloader;
session_start();
?>
<head>
    <meta name="viewport" content="width=device-width,initial-scale=1.0">
    <style type="text/css">
        * {
            margin: 1px;
        }
        input {
            display: block;
        }
    </style>
</head>
<body>
    <form action="" method="post">
        <select name="operator" autofocus="autofocus">
            <option value="+">+</option>
            <option value="-">-</option>
            <option value="/">/</option>
            <option value="*">*</option>
            <option value="%">%</option>
            <option value="√">√</option>
        </select><br>
        <input type="number" name="value" placeholder="ex: 1">        
        <button type="submit">=</button>
        <button type="submit" name="clear">C</button>
    </form>
</body>
<?php
$obj = new CalculatorController;

if (isset($_POST['value'])) {
    $obj->processRequest($_POST['operator'], (int)$_POST['value']);
}

if (isset($_POST['clear'])) {
    session_destroy();
}
 
if (!isset($_POST['clear'])) {
    $tmp = array_values($_SESSION['result']); 
    if ($_POST['operator'] == '√') {
        echo $_POST['operator'].' '.round($tmp[count($tmp) -2], 5).' = '.round(end($_SESSION['result']), 5);
    } elseif (count($_SESSION['result']) > 1 && $_POST['value'] !== "") {
        echo round($tmp[count($tmp) -2], 5).' '.$_POST['operator'].' '.$_POST['value'].' = '.round(end($_SESSION['result']), 5);
    } elseif (count($_SESSION['result']) > 1) {
        echo round($tmp[count($tmp) -2], 5).' '.$_POST['operator'].' 0 = '.round(end($_SESSION['result']), 5);        
    } else {
        echo '0 '.$_POST['operator'].' '.$_POST['value'].' = '.round(end($_SESSION['result']), 5);
    }
}
 